package tenid_authlib

/*
Engineering Ingegneria Informatica SpA licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

import (
	"encoding/json"
	"errors"
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"time"
)

var authorizationMap map[string]string

// The Map of Parameters contains as a Key the operation to authz and as Value a list of Domain
func Init(channelName string) error {
	//Log(DEBUG, AUTH_LIB+"Init function executing...", "")
	//authorizationMap = _authMap
	//Log(DEBUG, AUTH_LIB+"Authorization Map is: ", authorizationMap)
	ChannelName = channelName
	IdentityChaincodeName = TENIDENTITIES
	RoleChaincodeName = TENPOLICIES
	return nil
}

func Auth(APIstub shim.ChaincodeStubInterface) (string, error) {
	if ChannelName == "" {
		ChannelName = CHANNELNAME
	}
	_, args := APIstub.GetFunctionAndParameters()
	if len(args) == 0 {
		return "", errors.New(ValResp(400, ERROR_NUMBER_OF_ARGUMENTS))
	}
	Log(INFO, AUTH_LIB+" Parameter: ", args[0])
	var addr = args[0]
	if len(addr) == 0 {
		return "", errors.New(ValResp(400, ERROR_INVALID_IDENTIY))
	}
	var sign = ""
	if len(args) == 2 {
		sign = args[1]
	}
	authenticated, err := authenticate(APIstub, addr, sign)
	if err != nil {
		return "", errors.New(err.Error())
	}
	if authenticated {
		return addr, nil
		//authorize(APIstub, function, addr)
	}
	return "", errors.New(ValResp(401, ERROR_AUTHENTICATION_FAILED))
}

func authenticate(APIstub shim.ChaincodeStubInterface, addr string, sign string) (bool, error) {
	Log(DEBUG, AUTH_LIB+"authenticate function executing...", "")
	function, args := APIstub.GetFunctionAndParameters()
	var text = function + args[0]
	invokeArgs := [][]byte{}
	//verifySignature(addr, text, signature)
	invokeArgs = ToChaincodeArgs("verifySignature", addr, text, sign)
	Log(DEBUG, AUTH_LIB+" Invoke chaincode "+IdentityChaincodeName+" for user : ", addr)
	if ChannelName == "" {
		return false, errors.New(ERROR_CHANNEL_NAME_EMPTY)
	}
	response := APIstub.InvokeChaincode(IdentityChaincodeName, invokeArgs, ChannelName)
	Log(DEBUG, AUTH_LIB+"Invoke chaincode "+IdentityChaincodeName+" with ChaincodeResponse Message: ", response.Message)
	if response.Status != shim.OK {
		return false, errors.New(AUTH_LIB + "error into INVOKE: " + IdentityChaincodeName)
	}
	Log(INFO, AUTH_LIB+"response Payload: ", string(response.Payload))
	var verify = string(response.Payload)
	b := verify == "true"
	return b, nil
}

//NOT used at the moment
func authorize(APIstub shim.ChaincodeStubInterface, function string, user string) (bool, error) {
	Log(DEBUG, AUTH_LIB+"authorize function executing...", "")
	// If len(authorizationMap) == 0 Domain is not indicated and there aren't limits to operations
	if nil == authorizationMap || len(authorizationMap) == 0 {
		//return false, errors.New(ValResp(403, "Authorization Map is empty, please Initialize this map!"))
		return true, nil //If the map is empty you can avoid authorization by Policies
	}
	domainString := authorizationMap[function]
	// if domain= "" or domain = * all the users are authorized to use this function
	if domainString == "" || domainString == "*" {
		return true, nil
	}
	Log(DEBUG, AUTH_LIB+" Authorize invoke chaincode "+RoleChaincodeName+" getDomain", domainString)
	invokeArgs := [][]byte{}
	invokeArgs = ToChaincodeArgs("getDomain", domainString)
	response := APIstub.InvokeChaincode(RoleChaincodeName, invokeArgs, ChannelName)
	Log(DEBUG, AUTH_LIB+" Post invoke chaincode. ", "")
	if response.Status != shim.OK {
		return false, errors.New(ValResp(403, AUTH_LIB+"error into INVOKE getDomain in "+RoleChaincodeName))
	}
	Log(DEBUG, AUTH_LIB+"INVOKE chaincode "+RoleChaincodeName+" with ChaincodeResponse Payload: ", string(response.Payload))

	var domainResponse = ChaincodeResponse{}
	var domain = Domain{}
	err := json.Unmarshal(response.Payload, &domainResponse)
	if err != nil {
		return false, errors.New(ERROR_UNMARSHALL_DOMAIN_RESPONSE)
	}
	err = json.Unmarshal([]byte(domainResponse.Message), &domain)
	if err != nil {
		return false, errors.New(ERROR_UNMARSHALL_DOMAIN_RESPONSE)
	}
	Log(DEBUG, AUTH_LIB+"Domain retrieved is:", domain)
	if domain.Address == "" {
		return false, errors.New(ValResp(403, AUTH_LIB+"Domain for operation: "+function+" is not set, please Initialize the map!"))
	}
	Log(INFO, AUTH_LIB+" Domain passed to Authorize: ", domain.Address)
	Log(DEBUG, AUTH_LIB+" Authorize invoke chaincode Role getRole", "")
	invokeArgs = [][]byte{}
	invokeArgs = ToChaincodeArgs("getPolicy", domain.Address, user)
	response = APIstub.InvokeChaincode(RoleChaincodeName, invokeArgs, ChannelName)
	Log(DEBUG, AUTH_LIB+" Post invoke chaincode "+RoleChaincodeName+" with ChaincodeResponse Message: ", response.Message)
	if response.Status != shim.OK {
		return false, errors.New(ValResp(403, "ProtectIdentityLib error into INVOKE getPolicy in "+RoleChaincodeName))
	}
	Log(INFO, time.Now().Format(time.RFC3339)+" ProtectIdentityLib - response: ", response.String())
	Log(INFO, "IDENTITY ADDRESS "+user+" is AUTHORIZED to access the resource", "")
	return true, nil
}
