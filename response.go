package tenid_authlib

/*
Engineering Ingegneria Informatica SpA licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

import (
	"encoding/json"
)

// Response : Code Response
type Response struct {
	Header  `json:"header"`
	Message string `json:"message"`
}

// StringResp method for stringify the Json data
func StringResp(resp Response) string {

	var respByte []byte
	var err error
	respByte, err = json.Marshal(resp)
	if err != nil {
		return "500 - Error in Marshal of Response "
	}
	return string(respByte)
}

// ValResp method for stringify the Json data with input parameters
func ValResp(code int, desc string) string {
	var resp Response = Response{}
	resp.Code = code
	resp.Header.Description = desc
	resp.Message = ""
	return StringResp(resp)
}

// ValResp method for stringify the Json data with input parameters
func ValResponse(code int, desc string, mess string) string {
	var resp Response = Response{}
	resp.Code = code
	resp.Header.Description = desc
	resp.Message = mess
	return StringResp(resp)
}
