package main

import (
	"errors"
	"fmt"
	tenid_authlib "workspace/tenid-authlib"
)

func main() {
	err := errors.New(tenid_authlib.ValResponse(403, "cane", "mare"))
	fmt.Println(err.Error())

}
